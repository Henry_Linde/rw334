-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 28, 2016 at 08:19 
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Group`
--

-- --------------------------------------------------------

--
-- Table structure for table `claimStandby`
--

CREATE TABLE `claimStandby` (
  `sclaimID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `claimWorking`
--

CREATE TABLE `claimWorking` (
  `wclaimID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `date` date NOT NULL,
  `description` text NOT NULL,
  `timetaken` int(11) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE `receipt` (
  `receiptID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `file` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `userID` int(11) NOT NULL,
  `Username` text NOT NULL,
  `password` text NOT NULL,
  `Firstname` varchar(99) NOT NULL,
  `Lastname` varchar(99) NOT NULL,
  `Phone` varchar(12) NOT NULL,
  `Email` varchar(99) NOT NULL,
  `standbyRate` double NOT NULL,
  `workingRate` double NOT NULL,
  `balance` double NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `securityQ` text NOT NULL,
  `securityA` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `claimStandby`
--
ALTER TABLE `claimStandby`
  ADD PRIMARY KEY (`sclaimID`),
  ADD UNIQUE KEY `sclaimID` (`sclaimID`);

--
-- Indexes for table `claimWorking`
--
ALTER TABLE `claimWorking`
  ADD PRIMARY KEY (`wclaimID`),
  ADD UNIQUE KEY `wclaimID` (`wclaimID`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`receiptID`),
  ADD UNIQUE KEY `receiptID` (`receiptID`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `userID` (`userID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
