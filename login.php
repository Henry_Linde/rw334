<!DOCTYPE html>
<html>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<head>
<title>Login</title>
    <link href="login.css" rel="stylesheet"/>
</head>
<body class="body">
    <img id="background" src="images/background.jpg" height="100%" alt=""/>
    <div class="container">
    <div class="row">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
        <div id="grey">
            <?php
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $usr = $_POST['username'];
                    $pass = $_POST['password'];
                    $pass = md5($pass);
                    $remember;
                    if (isset($_POST['rememberme'])) {
                        $remember = $_POST['rememberme'];
                        if ($remember == TRUE) {
                            $remember = "yes";
                        } else {
                            $remember = "no";
                        }
                    } else {
                        $remember = "no";
                    }
                    
                    $conn = new mysqli("localhost", "root", "");
                    
                    if (!$conn) {
                        die("Connection failed: " . $mysqli_connect_error());
                    }    
                    
                    mysqli_select_db($conn, "DetailsDDB");
                    
                    $sql = "SELECT * FROM Users WHERE Username=\"". $usr ."\" AND Password=\"". $pass. "\"";
                    $res = mysqli_query($conn, $sql);
                    if (mysqli_num_rows($res) === 0) {
                        $errors = TRUE;
                    } else {
                        $errors = FALSE;
                    }
                    
                    $data = mysqli_fetch_assoc($res);
                    
                    mysqli_free_result($res);
                    echo "<h2>";
                    echo $usr;
                    echo "<br>";
                    echo "NOW IT IS HASHED PASS <br>";
                    echo $pass;
                    echo "<br>";
                    echo $remember;
                    echo "<br>";
                    echo "450ed2e34e0bf66f0aed387cf4585341";
                    echo "</h2>";
                }
            ?>
            <form class="form-signin" action="login.php" method="post">
                <h2 class="form-signin-heading">Please sign in</h2>
                <?php
                    if (isset($errors)) {
                        if ($errors == TRUE) {
                            echo "<p class=\'incorrect\'>Incorrect Login Details</p>";
                        }
                    }
                ?>
                <label for="inputUsername" class="sr-only">Username</label>
                    <input name="username" type="username" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input name="password" type="password" id="inputPassword" class="form-control"  placeholder="Password" required>
                <div class="checkbox">
                    <label>
                        <input name='rememberme' type="checkbox" name="remember" value="remember-me">   Remember me
                    </label>
                </div>
                <div class="forgot">
                    <label>
                        <a href="forgot.php">Forgot Password ?</a>
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>     
        </div>
        </div>
        <div class="col-sm-4"></div>
        </div>
        
        <div class="row">
            <div class="footer">
                <HR color="purple"/>
            </div>
        </div>
    </div> <!-- /container -->
</body>
</html>